import styled from 'styled-components'

const DashboardMainContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1.5rem;

  padding: 2.5rem;

  border-radius: 20px;
  background-color: #323232;
  color: #f8f8f8;
  box-shadow: 0 0.16rem 0.36rem 0 rgb(0 0 0 / 13%), 0 0.03rem 0.09rem 0 rgb(0 0 0 / 11%);

  width: 100%;
  max-width: 800px;
  min-height: 100px;
`

export default DashboardMainContainer
