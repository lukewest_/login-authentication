import React from 'react'

import {
  DashboardMain
} from '../organism'

const Layout = () => {
  return (
    <DashboardMain />
  )
}

export default Layout