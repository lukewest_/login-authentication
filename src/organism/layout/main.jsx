import React, { useEffect, useState } from 'react'
import { Route, Routes } from 'react-router-dom'
import { LoginContext } from '../../atom/layout/loginContext'

import {
  DashboardMainContainer,
} from '../../atom'
import {
  Dashboard,
  Login,
  Preferences
} from '../../pages'
import {
  useToken
} from '../../static'

const DashboardMain = () => {
  const { token, setToken } = useToken()
  const [user, setUser] = useState([])

  useEffect(() => {
    const loggedInUser = localStorage.getItem('user')
    if (loggedInUser) {
      setUser(JSON.parse(loggedInUser))
    }
  }, [])

  const handleLogout = () => {
    setUser([])
    setToken('')
    localStorage.clear()
  }

  if (!token) return <DashboardMainContainer><Login setToken={setToken} setUser={setUser} /></DashboardMainContainer>

  return (
    <LoginContext.Provider value={{ user }}>
      <DashboardMainContainer>
        <Routes>
          <Route path="/" element={<Dashboard handleLogout={handleLogout} />} />
          <Route path="/companies" element={<Preferences />} />
        </Routes>
      </DashboardMainContainer>
    </LoginContext.Provider>
  )
}

export default DashboardMain
