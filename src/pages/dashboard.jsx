import React, { useContext } from 'react'
import { LoginContext } from '../atom/layout/loginContext'

const Dashboard = ({
  handleLogout
}) => {
  const { user } = useContext(LoginContext)

  return (
    <>
      <h1>
        Dashboard
        <br />
        <small>Hello, { user.first_name }!</small>
      </h1>
      <button onClick={handleLogout}>Logout</button>
    </>
  )
}

export default Dashboard