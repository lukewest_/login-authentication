import React, { useState } from 'react'
import axios from 'axios'
import { environment } from '../static'

const Login = ({
  setToken,
  setUser
}) => {
  const [username, setUsername] = useState()
  const [password, setPassword] = useState()
  const [error, setError] = useState({
    global: '',
    username: '',
    password: ''
  })

  const getUser = (username, password) => {
    axios({
      method: 'GET',
      url: `${environment.apiRest}/users`
    }).then((response) => {
      const user = response.data.filter((f => f.username === username && f.password === password))
      if (user.length > 0) {
        if (user[0].status) {
          setUser(user[0])
          // set localstorage
          localStorage.setItem('user', JSON.stringify(user[0]))
          //
          setToken(user[0].token)
        } else {
          setError({
            ...error,
            globalError: 'This account has been disabled, please click on the link below to reactivate your account'
          })
        }
      } else {
        setError({
          ...error,
          globalError: 'We do not recognise this username and password, please try again or click here to create a new user'
        })
      }
    }).catch((err) => {
      setError({
        ...error,
        globalError: err.message
      })
    })
  }

  const handleLoginValidation = (e, name, value) => {
    e.preventDefault()

    switch (name) {
      case 'txtUsername':
          if (value.length === 0) {
            setError({
              ...error,
              username: 'Please enter your username'
            })
          } else {
            setError({ ...error, username: '' })
            setUsername(value)
          }
        break
      case 'txtPassword':
        if (value.length === 0) {
          setError({
            ...error,
            password: 'Please enter your password'
          })
        } else if (!new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/).test(value)) {
          setError({
            ...error,
            password: 'Password should contain uppercase, lowercase and numbers'
          })
        } else {
          setError({ ...error, password: '' })
          setPassword(value)
        }
        break
      default:
        break
    }
  }

  const handleLogin = e => {
    e.preventDefault()

    if (username && password) getUser(username, password)
    else setError({
      ...error,
      globalError: 'Please enter a username and password to login'
    })
  }

  const handleChange = (e) => {
    e.preventDefault()

    let name = e.target.name
    let value = e.target.value

    handleLoginValidation(e, name, value)
  }

  return (
    <>
      <h1>Login</h1>
      { error.globalError && <div className="error">{ error.globalError }</div> }
      <form onSubmit={handleLogin}>
        <div className="form-control">
          <label>Username</label>
          <input type="text" name="txtUsername" onChange={e => handleChange(e)} />
          { error.username && <div className="error">{ error.username }</div> }
        </div>
        <div className="form-control">
          <label>Password</label>
          <input type="password" name="txtPassword" onChange={e => handleChange(e)} />
          { error.password && <div className="error">{ error.password }</div> }
        </div>
        <div className="form-control">
          <button type="submit">
            Login
          </button>
        </div>
      </form>
    </>
  )
}

export default Login