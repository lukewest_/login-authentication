import Dashboard from './dashboard'
import Login from './login'
import Preferences from './preferences'

export {
  Dashboard,
  Login,
  Preferences
}