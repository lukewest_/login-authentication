import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Layout } from './template'
import { GlobalStyle } from './tokens'

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <GlobalStyle />
      <Layout />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
